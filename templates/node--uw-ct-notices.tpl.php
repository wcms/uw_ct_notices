<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <div class="notices_head">
    <?php if ($teaser): ?>
      <h3<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><img src="<?php echo base_path().drupal_get_path('module', 'uw_ct_notices'); ?>/images/<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->field_machine_name[LANGUAGE_NONE][0]['value']); ?>_small.png" width="32" height="32" alt="<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->name); ?>:" /><?php print $title; ?></a></h3>
    <?php elseif (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><img src="<?php echo base_path().drupal_get_path('module', 'uw_ct_notices'); ?>/images/<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->field_machine_name[LANGUAGE_NONE][0]['value']); ?>_big.png" width="48" height="48" alt="<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->name); ?>:" /><?php print $title; ?></a></h2>
    <?php else: ?>
      <h1<?php print $title_attributes; ?>><img src="<?php echo base_path().drupal_get_path('module', 'uw_ct_notices'); ?>/images/<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->field_machine_name[LANGUAGE_NONE][0]['value']); ?>_big.png" width="48" height="48" alt="<?php print check_plain($content['field_vocab_notices_type']['#items'][0]['taxonomy_term']->name); ?>:" /><?php print $title; ?></h1>
    <?php endif; ?>
    <?php if (!$teaser) { ?>
      <div><?php print render($content['field_date_time']); ?></div>
      <div><?php print render($content['field_location']); ?></div>
    <?php } ?>
    </div>

    <?php print $user_picture; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_category']);
      hide($content['field_vocab_notices_type']);
      if ($teaser) {
        print '<a href="'. $node_url . '">';
      }
      print render($content);
      if ($teaser) {
        print '</a>';
      }
      ?>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

    <?php if (!$teaser) {
      //generate the HTML we will use for the list of notice categories
      //note that the attributes match what was on the auto-generated links
      $field_category = '';
      foreach ($content['field_category']['#items'] as $term) {
        $link = l(t($term['taxonomy_term']->name), 'notices', array(
          'query' => array('dates' => 'All', 'type' => 'All', 'category' => $term['tid']),
          'attributes' => array('property' => 'rdfs:label skos:prefLabel', 'typeof' => 'skos:Concept'),
        ));
        $field_category .= '<div class="field-item">' . $link . '</div>';
      }
      ?>
    <div class="postinfo">

      <?php if (!empty($content['field_category'])): ?>
        <div class="notices_categories">Filed under: <?php print $field_category; ?></div>
      <?php endif; ?>      
      <?php if ($display_submitted): ?>
        <div class="submitted">Filed by: <?php print $name; ?>  on <span property="dc:date dc:created" content="<?php print format_date($node->created, 'custom', "c"); ?>" datatype="xsd:dateTime"><?php print format_date($node->created, 'custom', 'F j, Y'); ?><span></div>
      <?php endif; ?>
    
    </div>
    <?php } ?>

  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
