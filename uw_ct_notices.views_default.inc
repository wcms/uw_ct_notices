<?php
/**
 * @file
 * uw_ct_notices.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_notices_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uw_notices';
  $view->description = 'Listing pages for the Notices content type.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Notices';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Notices';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '7';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Relationship: Content: Notices type (field_vocab_notices_type) */
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['id'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['table'] = 'field_data_field_vocab_notices_type';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['field'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['required'] = TRUE;
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Content: Notices type */
  $handler->display->display_options['fields']['field_vocab_notices_type']['id'] = 'field_vocab_notices_type';
  $handler->display->display_options['fields']['field_vocab_notices_type']['table'] = 'field_data_field_vocab_notices_type';
  $handler->display->display_options['fields']['field_vocab_notices_type']['field'] = 'field_vocab_notices_type';
  $handler->display->display_options['fields']['field_vocab_notices_type']['label'] = '';
  $handler->display->display_options['fields']['field_vocab_notices_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_vocab_notices_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vocab_notices_type']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_vocab_notices_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[title] [[field_vocab_notices_type]]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Date/time */
  $handler->display->display_options['fields']['field_date_time']['id'] = 'field_date_time';
  $handler->display->display_options['fields']['field_date_time']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['fields']['field_date_time']['field'] = 'field_date_time';
  $handler->display->display_options['fields']['field_date_time']['label'] = '';
  $handler->display->display_options['fields']['field_date_time']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_time']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_time']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Short summary */
  $handler->display->display_options['fields']['field_short_summary']['id'] = 'field_short_summary';
  $handler->display->display_options['fields']['field_short_summary']['table'] = 'field_data_field_short_summary';
  $handler->display->display_options['fields']['field_short_summary']['field'] = 'field_short_summary';
  $handler->display->display_options['fields']['field_short_summary']['label'] = '';
  $handler->display->display_options['fields']['field_short_summary']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_short_summary']['alter']['text'] = '[field_date_time]

[field_short_summary]

For full details, please visit the site.';
  $handler->display->display_options['fields']['field_short_summary']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_short_summary']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_short_summary']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_short_summary']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_short_summary']['type'] = 'text_plain';
  /* Field: Content: Updated date */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'node';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['changed']['date_format'] = 'custom';
  $handler->display->display_options['fields']['changed']['custom_date_format'] = 'r';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Date only */
  $handler->display->display_options['sorts']['raw_sort']['id'] = 'raw_sort';
  $handler->display->display_options['sorts']['raw_sort']['table'] = 'views_raw_sql';
  $handler->display->display_options['sorts']['raw_sort']['field'] = 'raw_sort';
  $handler->display->display_options['sorts']['raw_sort']['ui_name'] = 'Date only';
  $handler->display->display_options['sorts']['raw_sort']['raw_sql'] = 'DATE(field_date_time_value)';
  /* Sort criterion: Taxonomy term: Urgent (field_urgent) */
  $handler->display->display_options['sorts']['field_urgent_value']['id'] = 'field_urgent_value';
  $handler->display->display_options['sorts']['field_urgent_value']['table'] = 'field_data_field_urgent';
  $handler->display->display_options['sorts']['field_urgent_value']['field'] = 'field_urgent_value';
  $handler->display->display_options['sorts']['field_urgent_value']['relationship'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['sorts']['field_urgent_value']['order'] = 'DESC';
  /* Sort criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['sorts']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['sorts']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['sorts']['field_date_time_value']['field'] = 'field_date_time_value';
  /* Contextual filter: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['arguments']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['arguments']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['arguments']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['arguments']['field_date_time_value']['default_argument_type'] = 'date';
  $handler->display->display_options['arguments']['field_date_time_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_date_time_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_date_time_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_date_time_value']['year_range'] = '-1000:+1000';
  $handler->display->display_options['arguments']['field_date_time_value']['granularity'] = 'year';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_notices' => 'uw_ct_notices',
  );

  /* Display: Listing Page */
  $handler = $view->new_display('page', 'Listing Page', 'uw_notices_listing');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<p>No notices found.</p>';
  $handler->display->display_options['empty']['area']['format'] = 'uw_tf_standard';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_notices' => 'uw_ct_notices',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Global: Composite Filter */
  $handler->display->display_options['filters']['composite_views_filter']['id'] = 'composite_views_filter';
  $handler->display->display_options['filters']['composite_views_filter']['table'] = 'views';
  $handler->display->display_options['filters']['composite_views_filter']['field'] = 'composite_views_filter';
  $handler->display->display_options['filters']['composite_views_filter']['group'] = 1;
  $handler->display->display_options['filters']['composite_views_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['composite_views_filter']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['label'] = 'Dates';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['operator'] = 'composite_views_filter_op';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['identifier'] = 'dates';
  $handler->display->display_options['filters']['composite_views_filter']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['composite_views_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['groups'] = 'All|- Any -
Upcoming|Current & upcoming
Past|Past';
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['default_group'] = 'Upcoming';
  $handler->display->display_options['filters']['composite_views_filter']['composite_views_filter']['classification'] = array(
    'field_date_time_value' => 'Upcoming',
    'field_date_time_value_1' => 'Past',
  );
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_date_time_value']['group'] = 1;
  $handler->display->display_options['filters']['field_date_time_value']['expose']['operator_id'] = 'field_date_time_value_op';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['label'] = 'Date/time -  start date (field_date_time)';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['operator'] = 'field_date_time_value_op';
  $handler->display->display_options['filters']['field_date_time_value']['expose']['identifier'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value']['year_range'] = '-0:+3';
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value_1']['id'] = 'field_date_time_value_1';
  $handler->display->display_options['filters']['field_date_time_value_1']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value_1']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value_1']['operator'] = '<';
  $handler->display->display_options['filters']['field_date_time_value_1']['group'] = 1;
  $handler->display->display_options['filters']['field_date_time_value_1']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value_1']['year_range'] = '-10:+0';
  /* Filter criterion: Content: Notices type (field_vocab_notices_type) */
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['id'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['table'] = 'field_data_field_vocab_notices_type';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['field'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['expose']['operator_id'] = 'field_vocab_notices_type_tid_op';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['expose']['operator'] = 'field_vocab_notices_type_tid_op';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_vocab_notices_type_tid']['vocabulary'] = 'uw_vocab_notices_types';
  /* Filter criterion: Content: Category (field_category) */
  $handler->display->display_options['filters']['field_category_tid']['id'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['table'] = 'field_data_field_category';
  $handler->display->display_options['filters']['field_category_tid']['field'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator_id'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['identifier'] = 'category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    16 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_category_tid']['vocabulary'] = 'uw_vocab_notices_categories';
  $handler->display->display_options['path'] = 'notices';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Notices';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: RSS Feed */
  $handler = $view->new_display('feed', 'RSS Feed', 'uw_notices_feed');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['style_options']['description'] = 'A list of notices.';
  $handler->display->display_options['row_plugin'] = 'rss_fields';
  $handler->display->display_options['row_options']['title_field'] = 'title';
  $handler->display->display_options['row_options']['link_field'] = 'path';
  $handler->display->display_options['row_options']['description_field'] = 'field_short_summary';
  $handler->display->display_options['row_options']['creator_field'] = 'name';
  $handler->display->display_options['row_options']['date_field'] = 'changed';
  $handler->display->display_options['row_options']['guid_field_options'] = array(
    'guid_field' => 'path',
    'guid_field_is_permalink' => 1,
  );
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Notices type (field_vocab_notices_type) */
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['id'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['table'] = 'field_data_field_vocab_notices_type';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['field'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['relationships']['field_vocab_notices_type_tid']['required'] = TRUE;
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_notices' => 'uw_ct_notices',
  );
  /* Filter criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['filters']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['filters']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['filters']['field_date_time_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_date_time_value']['default_date'] = '12AM today';
  $handler->display->display_options['filters']['field_date_time_value']['year_range'] = '-0:+3';
  $handler->display->display_options['path'] = 'notices/notices.xml';
  $handler->display->display_options['displays'] = array(
    'uw_notices_listing' => 'uw_notices_listing',
    'default' => 0,
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'uw_notices_block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['label'] = 'RSS & all notices links';
  $handler->display->display_options['footer']['area']['empty'] = TRUE;
  $handler->display->display_options['footer']['area']['content'] = '<a href="notices/notices.xml" class="notices_rss">RSS</a>
<a href="notices" class="notices_all">all notices</a>';
  $handler->display->display_options['footer']['area']['format'] = 'uw_tf_standard';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Date only */
  $handler->display->display_options['sorts']['raw_sort']['id'] = 'raw_sort';
  $handler->display->display_options['sorts']['raw_sort']['table'] = 'views_raw_sql';
  $handler->display->display_options['sorts']['raw_sort']['field'] = 'raw_sort';
  $handler->display->display_options['sorts']['raw_sort']['ui_name'] = 'Date only';
  $handler->display->display_options['sorts']['raw_sort']['order'] = 'DESC';
  $handler->display->display_options['sorts']['raw_sort']['raw_sql'] = 'DATE(field_date_time_value)';
  /* Sort criterion: Taxonomy term: Urgent (field_urgent) */
  $handler->display->display_options['sorts']['field_urgent_value']['id'] = 'field_urgent_value';
  $handler->display->display_options['sorts']['field_urgent_value']['table'] = 'field_data_field_urgent';
  $handler->display->display_options['sorts']['field_urgent_value']['field'] = 'field_urgent_value';
  $handler->display->display_options['sorts']['field_urgent_value']['relationship'] = 'field_vocab_notices_type_tid';
  $handler->display->display_options['sorts']['field_urgent_value']['order'] = 'DESC';
  /* Sort criterion: Content: Date/time -  start date (field_date_time) */
  $handler->display->display_options['sorts']['field_date_time_value']['id'] = 'field_date_time_value';
  $handler->display->display_options['sorts']['field_date_time_value']['table'] = 'field_data_field_date_time';
  $handler->display->display_options['sorts']['field_date_time_value']['field'] = 'field_date_time_value';
  $handler->display->display_options['sorts']['field_date_time_value']['order'] = 'DESC';
  $handler->display->display_options['block_description'] = 'Notices';
  $export['uw_notices'] = $view;

  return $export;
}
