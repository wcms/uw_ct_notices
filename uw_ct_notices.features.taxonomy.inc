<?php
/**
 * @file
 * uw_ct_notices.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_notices_taxonomy_default_vocabularies() {
  return array(
    'uw_vocab_notices_categories' => array(
      'name' => 'Notices categories',
      'machine_name' => 'uw_vocab_notices_categories',
      'description' => 'User-editable list of notice categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'uw_vocab_notices_types' => array(
      'name' => 'Notices types',
      'machine_name' => 'uw_vocab_notices_types',
      'description' => 'Types for the Notices content type.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
