<?php
/**
 * @file
 * uw_ct_notices.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_notices_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_notices content'.
  $permissions['create uw_ct_notices content'] = array(
    'name' => 'create uw_ct_notices content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_notices content'.
  $permissions['delete any uw_ct_notices content'] = array(
    'name' => 'delete any uw_ct_notices content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_notices content'.
  $permissions['delete own uw_ct_notices content'] = array(
    'name' => 'delete own uw_ct_notices content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_vocab_notices_categories'.
  $permissions['delete terms in uw_vocab_notices_categories'] = array(
    'name' => 'delete terms in uw_vocab_notices_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_vocab_notices_types'.
  $permissions['delete terms in uw_vocab_notices_types'] = array(
    'name' => 'delete terms in uw_vocab_notices_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_notices content'.
  $permissions['edit any uw_ct_notices content'] = array(
    'name' => 'edit any uw_ct_notices content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_notices content'.
  $permissions['edit own uw_ct_notices content'] = array(
    'name' => 'edit own uw_ct_notices content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_vocab_notices_categories'.
  $permissions['edit terms in uw_vocab_notices_categories'] = array(
    'name' => 'edit terms in uw_vocab_notices_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_vocab_notices_types'.
  $permissions['edit terms in uw_vocab_notices_types'] = array(
    'name' => 'edit terms in uw_vocab_notices_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_notices revision log entry'.
  $permissions['enter uw_ct_notices revision log entry'] = array(
    'name' => 'enter uw_ct_notices revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices authored by option'.
  $permissions['override uw_ct_notices authored by option'] = array(
    'name' => 'override uw_ct_notices authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices authored on option'.
  $permissions['override uw_ct_notices authored on option'] = array(
    'name' => 'override uw_ct_notices authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices promote to front page option'.
  $permissions['override uw_ct_notices promote to front page option'] = array(
    'name' => 'override uw_ct_notices promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices published option'.
  $permissions['override uw_ct_notices published option'] = array(
    'name' => 'override uw_ct_notices published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices revision option'.
  $permissions['override uw_ct_notices revision option'] = array(
    'name' => 'override uw_ct_notices revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_notices sticky option'.
  $permissions['override uw_ct_notices sticky option'] = array(
    'name' => 'override uw_ct_notices sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_ct_notices content'.
  $permissions['search uw_ct_notices content'] = array(
    'name' => 'search uw_ct_notices content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
