<?php
/**
 * @file
 * uw_ct_notices.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_notices_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'notices_all';
  $context->description = 'Items which appear on all notices and anything in the notices path.';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_ct_notices' => 'uw_ct_notices',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'notices' => 'notices',
        'notices/*' => 'notices/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_notices-uw_ct_notices_by_date' => array(
          'module' => 'uw_ct_notices',
          'delta' => 'uw_ct_notices_by_date',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Items which appear on all notices and anything in the notices path.');
  $export['notices_all'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'notices_home';
  $context->description = 'Items which only appear on the notices landing page.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'notices' => 'notices',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_notices-uw_ct_notices_public_feed' => array(
          'module' => 'uw_ct_notices',
          'delta' => 'uw_ct_notices_public_feed',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
    'breadcrumb' => '0',
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Items which only appear on the notices landing page.');
  $export['notices_home'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'notices_site_home';
  $context->description = 'Notices block on the site home page.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_notices-uw_notices_block' => array(
          'module' => 'views',
          'delta' => 'uw_notices-uw_notices_block',
          'region' => 'promo',
          'weight' => '-44',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Notices block on the site home page.');
  $export['notices_site_home'] = $context;

  return $export;
}
