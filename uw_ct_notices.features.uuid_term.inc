<?php
/**
 * @file
 * uw_ct_notices.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_notices_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Resolved urgent issue',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '0fced6ec-6e84-4ee0-96de-0798434bd8d1',
    'vocabulary_machine_name' => 'uw_vocab_notices_types',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'Resolved',
          'format' => NULL,
          'safe_value' => 'Resolved',
        ),
      ),
    ),
    'field_urgent' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Planned change',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '5733097b-125f-4daf-ba1e-096d29a473ad',
    'vocabulary_machine_name' => 'uw_vocab_notices_types',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'Planned',
          'format' => NULL,
          'safe_value' => 'Planned',
        ),
      ),
    ),
    'field_urgent' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Urgent issue',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'd1467e38-7a7a-4da8-bfdc-52fedf0afb4a',
    'vocabulary_machine_name' => 'uw_vocab_notices_types',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'Urgent',
          'format' => NULL,
          'safe_value' => 'Urgent',
        ),
      ),
    ),
    'field_urgent' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Tip or simple notice',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'd8180ac8-3d6b-449f-826c-833824f8be0b',
    'vocabulary_machine_name' => 'uw_vocab_notices_types',
    'field_machine_name' => array(
      'und' => array(
        0 => array(
          'value' => 'Tip',
          'format' => NULL,
          'safe_value' => 'Tip',
        ),
      ),
    ),
    'field_urgent' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
